#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDir>
#include <iostream>

QStringList& fileExtensions();
QString reportLines(const QStringList& folders, bool countEmptyLines);
QStringList subDirs(const QDir& dir);

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("cntln");
    parser.addVersionOption();
    parser.addHelpOption();
    parser.addPositionalArgument("dir(s)", "directories to be scanned");
    parser.addOptions({ { { "e", "empty" }, "count empty lines." } });
    parser.process(a);

    auto countEmptyLines = false;
    if(parser.isSet("e"))
        countEmptyLines = true;

    const QStringList args = parser.positionalArguments();
    if(args.size() < 1)
    {
        std::cerr << "wrong syntax - see 'cntln --help'";
        return -1;
    }

    QStringList folders{ args };
    for(const auto& dir : args)
        folders.append(subDirs(dir));
    auto report = reportLines(folders, countEmptyLines);

    std::cout << report.toStdString();

    return 0;
}

QString reportLines(const QStringList& folders, bool countEmptyLines)
{
    QString analysis("---\n");
    if(!countEmptyLines)
        analysis.append("Title: w/o empty lines\n"
                        "Shortcut: alt+1\n"
                        "---\n\n");
    else
        analysis.append("Title: with empty lines\n"
                        "Shortcut: alt+2\n"
                        "---\n\n");

    analysis.append("# CntLn Analysis ");
    if(countEmptyLines)
        analysis.append("including empty lines");
    else
        analysis.append("of non-empty lines");
    //analysis.append("\n\n## Evaluated directories\n");
    //analysis.append(folders.join("  \n"));
    QString details;
    details.append("\n## Lines of located C++ files (");
    details.append(fileExtensions().join(", ").remove('*'));
    details.append(")\n");

    QFile curFile;
    auto lineCounter = 0;
    auto fileCounter = 0;

    if(!fileExtensions().isEmpty())
    {
        for(const QDir curDir : folders)
        {
            auto fileList = curDir.entryList(fileExtensions());
            fileCounter += fileList.length();
            for(const auto& fileN : qAsConst(fileList))
            {
                auto filePath = curDir.path() + "/" + fileN;
                curFile.setFileName(filePath);
                curFile.open(QIODevice::ReadOnly | QIODevice::Text);
                auto allLines = QString(curFile.readAll());
                auto listOfLines = allLines.split(QStringLiteral("\n"));
                if(!countEmptyLines)
                    listOfLines.removeAll("");
                auto curLength = listOfLines.length();
                details.append(filePath + ": " + QString::number(curLength) + "  \n");
                lineCounter += curLength;
                curFile.close();
            }
        }
    }

    analysis.append("\n## Summary\n");
    analysis.append("total file count: " + QString::number(fileCounter) + "  \n");
    analysis.append("total line count: " + QString::number(lineCounter) + "  \n");

    analysis.append(details);

    return analysis;
}

QStringList& fileExtensions()
{
    static auto ret{ QStringList() << "*.cpp"
                                   << "*.tpp"
                                   << "*.h"
                                // << "*.cl"
                                // << "*.pro" << "*.pri" << ".ui" << ".md"
                   };
    return ret;
}

QStringList subDirs(const QDir& dir)
{
    auto ret = dir.entryList(QDir::Dirs);
    ret.removeOne(QStringLiteral("."));
    ret.removeOne(QStringLiteral(".."));
    for(auto& d : ret)
        d = dir.absolutePath() + "/" + d;

    QStringList subs;
    for(const auto& s : ret)
        subs.append(subDirs(s));
    ret.append(subs);

    return ret;
}
