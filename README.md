# CntLn - small Qt-based command line tool for counting lines of code

This tiny command line tool is a fork of
[LineCounter (Qt-based GUI)](https://gitlab.stimulate.ovgu.de/robert-frysch/LineCounter)
showing the number of lines of a code base.
It outputs a formatted string (markdown format) that gives an overview of the
files including C++ source code (filtering by file extension) and corresponding
lines of code.
